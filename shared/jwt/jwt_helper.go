package jwt

import (
	"time"

	"github.com/golang-jwt/jwt"
)

type JWTClaims struct {
	UserID int `json:"user_id"`
	jwt.StandardClaims
}

func JWTEncode(userID int) (string, error) {
	claims := &JWTClaims{
		userID,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(5 * time.Minute).Unix(),
		},
	}
	sign := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	token, err := sign.SignedString([]byte("secret"))
	if err != nil {
		return "", err
	}
	return token, nil
}

func JWTDecode(token string) (*JWTClaims, error) {
	claims := &JWTClaims{}
	_, err := jwt.ParseWithClaims(token, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte("secret"), nil
	})
	if err != nil {
		return nil, err
	}
	return claims, nil
}
