package middleware

import (
	"crud_echo/shared/jwt"
	"crud_echo/shared/response"
	"net/http"
	"strings"

	"github.com/labstack/echo/v4"
)

var UserID int

func Auth(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		token := c.Request().Header.Get("Authorization")
		if token == "" {
			return response.SetResponse(c, http.StatusUnauthorized, "unauthorized", nil)
		}
		tokenSplit := strings.Split(token, "Bearer ")
		if len(tokenSplit) < 2 {
			return response.SetResponse(c, http.StatusUnauthorized, "unauthorized", nil)
		}
		token = tokenSplit[1]
		claims, err := jwt.JWTDecode(token)
		if err != nil {
			return response.SetResponse(c, http.StatusUnauthorized, err.Error(), nil)
		}

		UserID = claims.UserID
		return next(c)
	}
}
