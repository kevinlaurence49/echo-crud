package dto

import (
	"regexp"

	validation "github.com/go-ozzo/ozzo-validation"
)

type UserDTO struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
	Address  string `json:"address"`
}

func (u UserDTO) Validation() error {
	err := validation.ValidateStruct(&u,
		validation.Field(&u.Username),
		validation.Field(&u.Email, validation.Required, validation.Match(regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`))),
		validation.Field(&u.Password, validation.Required),
		validation.Field(&u.Address))

	if err != nil {
		return err
	}
	return nil
}
