package usecase

import (
	"context"
	"crud_echo/config"
	"crud_echo/pkg/domain"
	"fmt"
	"io"
	"mime/multipart"
	"strings"

	"cloud.google.com/go/storage"
	firebase "firebase.google.com/go"
	"google.golang.org/api/option"
)

type FileHandlerUsecase struct {
	FileHandlerRepository domain.FileHandlerRepository
	BucketHandle          *storage.BucketHandle
}

func NewFileHandlerUsecase(fhr domain.FileHandlerRepository) domain.FileHandlerUsecase {
	opt := option.WithCredentialsFile("./config/serviceAccountKey.json")
	app, err := firebase.NewApp(context.Background(), nil, opt)
	if err != nil {
		panic(err)
	}

	client, err := app.Storage(context.Background())
	if err != nil {
		panic(err)
	}

	conf := config.GetConfig()
	bucket, err := client.Bucket(conf.BucketName)
	if err != nil {
		panic(err)
	}
	return &FileHandlerUsecase{
		FileHandlerRepository: fhr,
		BucketHandle:          bucket,
	}
}

func (fhu *FileHandlerUsecase) UploadFile(file *multipart.FileHeader) error {
	var fileHandler domain.FileHandler

	src, err := file.Open()
	if err != nil {
		panic(err)
	}
	defer src.Close()

	imgPath := file.Filename

	// Create a storage writer
	wc := fhu.BucketHandle.Object(imgPath).NewWriter(context.Background())

	// Copy the file to the storage writer
	if _, err := io.Copy(wc, src); err != nil {
		return err
	}
	if err := wc.Close(); err != nil {
		return err
	}

	filteredFilename := strings.ReplaceAll(file.Filename, " ", "%20")
	firebaseURL := fmt.Sprintf("https://firebasestorage.googleapis.com/v0/b/golang-crud-4ee51.appspot.com/o/%s?alt=media", filteredFilename)
	fileHandler.URL = firebaseURL

	if err := fhu.FileHandlerRepository.UploadFile(fileHandler); err != nil {
		return err
	}

	return nil
}

func (fhu *FileHandlerUsecase) GetFile(id int) (domain.FileHandler, error) {
	file, err := fhu.FileHandlerRepository.GetFile(id)
	if err != nil {
		return file, err
	}
	return file, nil
}
