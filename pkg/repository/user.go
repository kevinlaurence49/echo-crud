package repository

import (
	"crud_echo/pkg/domain"
	"database/sql"
)

type UserRepository struct {
	db *sql.DB
}

func NewUserRepository(db *sql.DB) domain.UserRepository {
	return &UserRepository{
		db: db,
	}
}

func (ur UserRepository) GetUsers() ([]domain.User, error) {
	query := `SELECT * FROM users`
	rows, err := ur.db.Query(query)
	var users []domain.User
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}

	for rows.Next() {
		var user domain.User
		err2 := rows.Scan(&user.Id, &user.Username, &user.Email, &user.Password, &user.Address)
		if err2 != nil {
			return nil, err2
		}
		users = append(users, user)
	}
	return users, err
}

func (ur UserRepository) GetUser(id int) (domain.User, error) {
	var user domain.User
	sql := `SELECT * FROM users WHERE id = $1`

	err := ur.db.QueryRow(sql, id).Scan(&user.Id, &user.Username, &user.Email, &user.Password, &user.Address)
	return user, err
}

func (ur UserRepository) CreateUser(req domain.User) error {
	sql := `INSERT INTO users (username, email, password, address) values ($1, $2, $3, $4)`
	stmt, err := ur.db.Prepare(sql)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err2 := stmt.Exec(req.Username, req.Email, req.Password, req.Address)
	if err2 != nil {
		return err2
	}
	return nil
}

func (ur UserRepository) UpdateUser(req domain.User) error {
	sql := `UPDATE users SET username = $1, email = $2, password = $3, address = $4 WHERE id = $5`
	stmt, err := ur.db.Prepare(sql)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err2 := stmt.Exec(req.Username, req.Email, req.Password, req.Address, req.Id)
	if err2 != nil {
		return err2
	}
	return nil
}

func (ur UserRepository) DeleteUser(id int) error {
	sql := `DELETE FROM users WHERE id = $1`
	stmt, err := ur.db.Prepare(sql)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err2 := stmt.Exec(id)
	if err2 != nil {
		return err2
	}
	return nil
}

func (ur UserRepository) GetUserByEmail(email string) (domain.User, error) {
	var user domain.User
	sql := `SELECT * FROM users WHERE email = $1`

	err := ur.db.QueryRow(sql, email).Scan(&user.Id, &user.Username, &user.Email, &user.Password, &user.Address)
	return user, err
}
