package repository

import (
	"crud_echo/pkg/domain"
	"database/sql"
	"errors"
	"regexp"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/suite"
)

type UserRepositoryTestSuite struct {
	suite.Suite
	db   *sql.DB
	mock sqlmock.Sqlmock
	repo domain.UserRepository
}

func (s *UserRepositoryTestSuite) SetupSuite() {
	// Initialize the SQL mock
	var err error
	s.db, s.mock, err = sqlmock.New()
	s.Require().NoError(err)

	// Create a UserRepository instance with the mock database
	s.repo = NewUserRepository(s.db)
}

func (s *UserRepositoryTestSuite) TearDownTest() {
	if s.db != nil {
		defer s.db.Close()
	}
}

func TestUserRepository(t *testing.T) {
	suite.Run(t, new(UserRepositoryTestSuite))
}

func (s *UserRepositoryTestSuite) TestGetUsers() {
	s.Run("successfully find a record", func() {
		s.mock.
			ExpectQuery(regexp.QuoteMeta(`SELECT * FROM users`)).
			WillReturnRows(
				sqlmock.
					NewRows([]string{
						"id",
						"name",
						"email",
						"password",
						"address",
					}).
					AddRow(
						1,
						"John Doe",
						"johndoe@gmail.com",
						"12345",
						"Jakarta",
					),
			)

		res, err := s.repo.GetUsers()
		s.NotNil(res)
		s.Nil(err)
		s.Equal(1, len(res))
	})

	s.Run("fail to find record due to error", func() {
		s.mock.
			ExpectQuery(regexp.QuoteMeta(`SELECT * FROM users`)).
			WillReturnError(errors.New("error"))

		res, err := s.repo.GetUsers()
		s.Nil(res)
		s.NotNil(err)
	})

	s.Run("fail to find record due to no existing data", func() {
		s.mock.
			ExpectQuery(regexp.QuoteMeta(`SELECT * FROM users`)).
			WillReturnError(sql.ErrNoRows)

		res, err := s.repo.GetUsers()
		s.Nil(res)
		s.Nil(err)
	})

}
