package repository

import (
	"crud_echo/pkg/domain"
	"crud_echo/shared/utils/cache"
	"database/sql"
	"encoding/json"
	"fmt"
)

type StudentRepository struct {
	db    *sql.DB // nil
	cache cache.Cacheable
}

func NewStudentRepository(db *sql.DB, cache cache.Cacheable) domain.StudentRepository {
	return &StudentRepository{
		db:    db,
		cache: cache,
	}
}

func (sr StudentRepository) GetStudents() ([]domain.Student, error) {
	sql := `SELECT * FROM students`
	rows, err := sr.db.Query(sql)
	var students []domain.Student
	for rows.Next() {
		var student domain.Student
		err2 := rows.Scan(&student.Id, &student.Fullname, &student.Address, &student.Birthdate, &student.Class, &student.Batch, &student.SchoolName)
		if err2 != nil {
			return nil, err2
		}
		students = append(students, student)
	}
	return students, err
}

func (sr StudentRepository) GetStudent(id int) (domain.Student, error) {
	var student domain.Student
	studentBytes, _ := sr.cache.Get(fmt.Sprintf("student:%v", id))

	if studentBytes != nil {
		_ = json.Unmarshal(studentBytes, &student)
		return student, nil
	}
	sql := `SELECT * FROM students WHERE id = $1`

	err := sr.db.QueryRow(sql, id).Scan(&student.Id, &student.Fullname, &student.Address, &student.Birthdate, &student.Class, &student.Batch, &student.SchoolName)

	sr.cache.Set(fmt.Sprintf("student:%v", id), student, 60)

	return student, err
}

func (sr StudentRepository) CreateStudent(req domain.Student) error {
	sql := `INSERT INTO students (fullname, address, birthdate, class, batch, school_name) values ($1, $2, $3, $4, $5, $6)`
	stmt, err := sr.db.Prepare(sql)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err2 := stmt.Exec(req.Fullname, req.Address, req.Birthdate, req.Class, req.Batch, req.SchoolName)
	if err2 != nil {
		return err2
	}
	return nil
}

func (sr StudentRepository) UpdateStudent(req domain.Student) error {
	sql := `UPDATE students SET fullname = $1, address = $2, birthdate = $3, class = $4, batch = $5, school_name = $6 WHERE id = $7`
	stmt, err := sr.db.Prepare(sql)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err2 := stmt.Exec(req.Fullname, req.Address, req.Birthdate, req.Class, req.Batch, req.SchoolName, req.Id)
	if err2 != nil {
		return err2
	}
	return nil
}

func (sr StudentRepository) DeleteStudent(id int) error {
	sql := `DELETE FROM students WHERE id = $1`
	stmt, err := sr.db.Prepare(sql)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err2 := stmt.Exec(id)
	if err2 != nil {
		return err2
	}
	return nil
}
