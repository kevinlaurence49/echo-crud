package repository

import (
	"crud_echo/pkg/domain"
	"database/sql"
)

type FileHandlerRepository struct {
	db *sql.DB
}

func NewFileHandlerRepository(db *sql.DB) *FileHandlerRepository {
	return &FileHandlerRepository{db}
}

func (fhr *FileHandlerRepository) UploadFile(file domain.FileHandler) error {
	sql := `INSERT INTO files (url) VALUES ($1)`
	stmt, err := fhr.db.Prepare(sql)
	if err != nil {
		return err
	}

	defer stmt.Close()
	_, err2 := stmt.Exec(file.URL)
	if err2 != nil {
		return err2
	}

	return nil
}

func (fhr *FileHandlerRepository) GetFile(id int) (domain.FileHandler, error) {
	var file domain.FileHandler
	sql := `SELECT * FROM files WHERE id=$1`
	err := fhr.db.QueryRow(sql, id).Scan(&file.ID, &file.URL)
	if err != nil {
		return file, err
	}

	return file, nil
}
