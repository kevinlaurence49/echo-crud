package router

import (
	"crud_echo/pkg/controller"
	"crud_echo/pkg/repository"
	"crud_echo/pkg/usecase"
	"database/sql"

	utilRedis "crud_echo/shared/utils/redis"

	"github.com/gomodule/redigo/redis"
	"github.com/labstack/echo/v4"
)

func NewStudentRouter(e *echo.Echo, g *echo.Group, db *sql.DB, redisPool *redis.Pool) {
	cache := utilRedis.NewClient(redisPool)

	sr := repository.NewStudentRepository(db, cache)
	su := usecase.NewStudentUsecase(sr)
	sc := &controller.StudentControler{
		StudentUsecase: su,
	}

	g.GET("/student", sc.GetStudents)
	g.GET("/student/:id", sc.GetStudent)
	g.POST("/student", sc.CreateStudent)
	g.PUT("/student/:id", sc.UpdateStudent)
	g.DELETE("/student/:id", sc.DeleteStudent)
}
