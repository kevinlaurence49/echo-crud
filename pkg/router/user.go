package router

import (
	"crud_echo/pkg/controller"
	"crud_echo/pkg/repository"
	"crud_echo/pkg/usecase"

	"crud_echo/shared/middleware"
	"database/sql"

	"github.com/labstack/echo/v4"
)

func NewUserRouter(e *echo.Echo, g *echo.Group, db *sql.DB) {
	ur := repository.NewUserRepository(db)
	uu := usecase.NewUserUsecase(ur)
	uc := &controller.UserController{
		UserUsecase: uu,
	}
	g.POST("/login", uc.Login)

	g.Use(middleware.Auth)
	g.GET("/users", uc.GetUsers)
	g.POST("/users", uc.CreateUser)
	g.GET("/users/:id", uc.GetUser)
	g.PUT("/users/:id", uc.UpdateUser)
	g.DELETE("/users/:id", uc.DeleteUser)
	g.GET("/users/profile", uc.GetUserProfile)
}
