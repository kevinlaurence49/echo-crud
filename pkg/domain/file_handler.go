package domain

import "mime/multipart"

type FileHandler struct {
	ID  int    `json:"id"`
	URL string `json:"url"`
}

type FileHandlerRepository interface {
	UploadFile(file FileHandler) error
	GetFile(id int) (FileHandler, error)
}

type FileHandlerUsecase interface {
	UploadFile(file *multipart.FileHeader) error
	GetFile(id int) (FileHandler, error)
}
