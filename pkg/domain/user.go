package domain

import "crud_echo/pkg/dto"

// User represents the user for this application
// swagger:model
type User struct {
	// the id for this user
	//
	// required: true
	// min: 1
	Id int `json:"id"`

	// the username for this user
	// required: true
	// min length: 3
	Username string `json:"username"`

	// the email for this user
	//
	// required: true
	// example: user@provider.net
	Email string `json:"email"`

	// the password for this user
	//
	// required: true
	// example: 12345
	Password string `json:"password"`

	// the address for this user
	//
	// required: true
	// example: user@provider.net
	Address string `json:"address"`
}

type UserRepository interface {
	GetUsers() ([]User, error)
	GetUser(id int) (User, error)
	CreateUser(req User) error
	UpdateUser(req User) error
	DeleteUser(id int) error
	GetUserByEmail(email string) (User, error)
}

type UserUsecase interface {
	GetUsers() ([]User, error)
	GetUser(id int) (User, error)
	CreateUser(req dto.UserDTO) error
	UpdateUser(req dto.UserDTO, id int) error
	DeleteUser(id int) error
	GetUserByEmail(email string) (User, error)
}
