package controller

import (
	"crud_echo/pkg/domain"
	"crud_echo/pkg/dto"
	"crud_echo/shared/jwt"
	"crud_echo/shared/middleware"
	"crud_echo/shared/response"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"golang.org/x/crypto/bcrypt"
)

type UserController struct {
	UserUsecase domain.UserUsecase
}

func (uc *UserController) GetUsers(c echo.Context) error {
	// swagger:operation GET /user users getUsers
	//
	// fetch all users
	//
	// ---
	// produces:
	// - application/json
	// responses:
	//   '200':
	//     description: array of user response
	//     schema:
	//       type: array
	//       items:
	//         "$ref": "#/definitions/User"

	resp, err := uc.UserUsecase.GetUsers()
	if err != nil {
		return response.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return response.SetResponse(c, http.StatusOK, "success", resp)
}

func (uc *UserController) CreateUser(c echo.Context) error {

	// swagger:operation POST /user users createUser
	//
	// endpoint to create user
	//
	// ---
	// produces:
	// - application/json
	// parameters:
	// - name: body
	//   in: body
	//   required: true
	//   schema:
	//     "$ref": "#/definitions/User"
	// responses:
	//   '200':
	//     description: user response

	var userdto dto.UserDTO
	if err := c.Bind(&userdto); err != nil {
		return response.SetResponse(c, http.StatusBadRequest, "bad request", nil)
	}
	if err := userdto.Validation(); err != nil {
		return response.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}
	if _, err := uc.UserUsecase.GetUserByEmail(userdto.Email); err == nil {
		return response.SetResponse(c, http.StatusBadRequest, "user already exists", nil)
	}
	passwordHash, _ := bcrypt.GenerateFromPassword([]byte(userdto.Password), bcrypt.DefaultCost)
	userdto.Password = string(passwordHash)
	if err := uc.UserUsecase.CreateUser(userdto); err != nil {
		return response.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return response.SetResponse(c, http.StatusOK, "success", nil)
}

func (uc *UserController) GetUser(c echo.Context) error {
	// swagger:operation GET /user/{:id} users getUser
	//
	// get one user with id
	//
	// ---
	// produces:
	// - application/json
	//
	// responses:
	//   '200':
	//     description: user response

	id, _ := strconv.Atoi(c.Param("id"))
	resp, err := uc.UserUsecase.GetUser(id)
	if err != nil {
		return response.SetResponse(c, http.StatusNotFound, "id user not found", nil)
	}
	return response.SetResponse(c, http.StatusOK, "success", resp)
}

func (uc *UserController) UpdateUser(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	if _, err := uc.UserUsecase.GetUser(id); err != nil {
		return response.SetResponse(c, http.StatusNotFound, "id user not found", nil)
	}

	var userdto dto.UserDTO
	if err := c.Bind(&userdto); err != nil {
		return response.SetResponse(c, http.StatusBadRequest, "bad request", nil)
	}
	if err := userdto.Validation(); err != nil {
		return response.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}
	passwordHash, _ := bcrypt.GenerateFromPassword([]byte(userdto.Password), bcrypt.DefaultCost)
	userdto.Password = string(passwordHash)
	if err := uc.UserUsecase.UpdateUser(userdto, id); err != nil {
		return response.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return response.SetResponse(c, http.StatusOK, "success", nil)
}

func (uc *UserController) DeleteUser(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	if _, err := uc.UserUsecase.GetUser(id); err != nil {
		return response.SetResponse(c, http.StatusNotFound, "id user not found", nil)
	}

	if err := uc.UserUsecase.DeleteUser(id); err != nil {
		return response.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return response.SetResponse(c, http.StatusOK, "success", nil)
}

func (uc *UserController) Login(c echo.Context) error {
	var userdto dto.UserDTO
	if err := c.Bind(&userdto); err != nil {
		return response.SetResponse(c, http.StatusBadRequest, "bad request", nil)
	}
	if err := userdto.Validation(); err != nil {
		return response.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}
	user, err := uc.UserUsecase.GetUserByEmail(userdto.Email)
	if err != nil {
		return response.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	if user == (domain.User{}) {
		return response.SetResponse(c, http.StatusBadRequest, "user not found", nil)
	}

	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(userdto.Password)); err != nil {
		return response.SetResponse(c, http.StatusBadRequest, "wrong password", nil)
	}
	token, err := jwt.JWTEncode(user.Id)
	if err != nil {
		return response.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return response.SetResponse(c, http.StatusOK, "success", token)
}

func (uc *UserController) GetUserProfile(c echo.Context) error {
	resp, err := uc.UserUsecase.GetUser(middleware.UserID)
	if err != nil {
		return response.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return response.SetResponse(c, http.StatusOK, "success", resp)
}
