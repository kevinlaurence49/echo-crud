package cmd

import (
	"context"
	"crud_echo/config"
	"crud_echo/pkg/router"
	"crud_echo/shared/db"
	"log"

	"github.com/gomodule/redigo/redis"
	"github.com/labstack/echo/v4"

	utilRedis "crud_echo/shared/utils/redis"
)

func RunServer() {
	e := echo.New()
	g := e.Group("v1")
	Apply(e, g)
	e.Logger.Error(e.Start(":5000"))
}

func Apply(e *echo.Echo, g *echo.Group) {
	conf := config.GetConfig()
	redisPool := buildRedisPool(conf)

	db := db.NewInstanceDb()
	router.NewStudentRouter(e, g, db, redisPool)
	router.NewUserRouter(e, g, db)
	router.NewFileHandlerRouter(e, g, db)
}

func buildRedisPool(cfg config.Configuration) *redis.Pool {
	cachePool := utilRedis.NewPool(cfg.RedisAddress, cfg.RedisPassword)

	ctx := context.Background()
	_, err := cachePool.GetContext(ctx)

	if err != nil {
		panic(err)
	}

	log.Print("redis successfully connected!")
	return cachePool
}
